<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/demo/1', 'ApiController@livre1')->name('demo1');
Route::get('/demo/2', 'ApiController@livre2')->name('demo2');
Route::get('/demo/3', 'ApiController@livre3')->name('demo3');
Route::get('/demo', 'ApiController@retour')->name('retour');



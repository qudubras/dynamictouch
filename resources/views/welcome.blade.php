<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
           

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center">

            <div class="content">
                <a href="{{ route('retour')}}"><img src="http://www.chequehavas.com/chvs_content/uploads/2015/10/0rdm_logo-cultura-ok_redim-2.jpg"></a>
                <h2>À la une:</h2>
                <div class="links">
                    <a type="button" class="btn" href="{{ route('demo1')}}"><img src="http://t2.gstatic.com/images?q=tbn:ANd9GcRDPqrKPg0iRkmV3H9JvHADWmb5y-hD_sIJcxw2f6AgduLVCGmc" width="250px"></a>
                    <a type="button" class="btn" href="{{ route('demo2')}}"><img src="https://images-na.ssl-images-amazon.com/images/I/514YB0d4yRL._SY344_BO1,204,203,200_.jpg" width="270px"></a>
                    <a type="button" class="btn" href="{{ route('demo3')}}"><img src="https://www.babelio.com/couv/CVT_Le-roi-chocolat_233.jpg" width="210px"></a>

                </div>
            </div>
        </div>
    </body>
</html>
